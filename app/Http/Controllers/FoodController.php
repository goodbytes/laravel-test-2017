<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FoodController extends Controller
{
    public function create() {
    	return view("food/create");
    }

    public function handleCreate(Request $request) {
    	
    	return view("food/create", $data);
    }

    public function list($filter = null) {
        // https://laravel.com/docs/5.5/routing#parameters-optional-parameters
    	return view("food/list");
    }
}
