@extends('layout')

@section('title')
	All Food
@endsection

@section('content')
	<nav aria-label="breadcrumb" role="navigation">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item all"><a href="/food/list/expensive">All</a></li>
	    <li class="breadcrumb-item cheap"><a href="/food/list/cheap">Cheap (-30€)</a></li>
	    <li class="breadcrumb-item expensive"><a href="/food/list/expensive">Expensive (+30€)</a></li>
	  </ol>
	</nav>

	<ul class="list-group">
	  <li class="list-group-item">French Fries: 54€</li>
	  <li class="list-group-item">Show all food here in a loop</li>
	  <li class="list-group-item">But make sure the filters above work</li>
	</ul>
@endsection