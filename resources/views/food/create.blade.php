@extends('layout')

{{-- Take a look at the layout, and use the corresponding sections to add a title to your page --}}


@section('content')
	<form method="post">
 	 <div class="form-group">
	    <label for="title">Title</label>
	    <input type="text" class="form-control" id="title" name="title" placeholder="The name of your food">
	    <small id="titleHelp" class="form-text text-muted">Try "Lasagna", "Steak Robespierre"</small>
	  </div>
	  <div class="form-group">
	    <label for="price">Price</label>
	    <input type="number" class="form-control" name="price" id="price" placeholder="Price">
	  </div>
	  
	  <button type="submit" class="btn btn-primary">Add food</button>

	  {{ csrf_field() }}
	</form>

	<div class="row" style="margin-top: 2em;">
		<div class="col-sm-12">
		<ul class="list-group">
		  <li class="list-group-item">French Fries: 54€</li>
		  <li class="list-group-item">Add new food + price at the top of this list, like the item above.</li>
		  <li class="list-group-item">I'm a pizza in a list-item. Loop over your food items here.</li>
		</ul>
		</div>
	</div>



@endsection