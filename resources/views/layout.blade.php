<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>@yield('title')</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
	<style>
		.instructions
		{
			font-size: 0.8em;
			border: 1px solid #5ef550;
			border-radius: 4px;
			padding: 1em;
			margin-top: 2em;
		}
	</style>
</head>
<body>
	<div class="container">
		<h1>@yield('title')</h1>
	
		@yield("content")


		<div class="instructions">
			<div class="row">
				<div class="col-sm-12">
					<h5>Instructions</h5>
					<ul>
						<li>Update to the latest version of https://bitbucket.org/goodbytes/laravel-test-2017</li>
						<li>Start by taking a look at /tests/Browser/CreateFoodTest.php</li>
						<li>Do not change these tests in any way</li>
						<li>Write your code in order to make the test complete succesfully</li>
						<li>Done? Complete columns C, D, E, F <a href="https://docs.google.com/spreadsheets/d/1mzCm1xNHdNEQz1ooJS3sV7fU99Rf3ZlvL5OXEb_U6hs/edit?usp=sharing">in this google document</a></li>
						<li>Note: not entering your results correctly will result in a fraude score for your whole semester</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</body>
</html>