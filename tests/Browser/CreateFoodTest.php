<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Faker\Factory as Faker;

class CreateFoodTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {

            // Our Testing Journey begins here
            $faker = Faker::create(); 
            $foodTitle = $faker->sentence(2);                   // we'll create a fake food title of two words
            $foodPrice = $faker->numberBetween(2, 44);          // with a fake price between 2 and 44 Euros
            $checkTitle = $foodTitle . ": " . $foodPrice . "€"; // output format = "French Fries: 44€"

            // in the test below, we'll visit the /food/create form and enter the fake data we just generated
            $browser->visit('/food/create')
                    ->assertTitle('Create Food')                // the title of the page should be 'Create Food'
                    ->type('title', $foodTitle)                 // fill up the form here
                    ->type('price', $foodPrice)                 // and here
                    ->press('Add food')                         // and press the save button
                                                                // the list should then display this new product on top
                    ->assertSeeIn(".list-group li:first-child", $checkTitle);

            // in the test below, we'll visit the /food/list page and see if our food is shown in the list 
            $browser->visit('/food/list')
                    ->assertTitle('All Food')                   // the page title should be 'All Food'
                    ->assertSeeIn(".list-group", $checkTitle);  // and our newly added food should be shown in the list

            // next, well try out the filter options to see if you can filter items by price
            if( $foodPrice < 30 ) {
                $browser->visit('/food/list/expensive')         // don't show items over 30€ when clicking the 'cheap' link
                    ->assertDontSeeIn(".list-group", $checkTitle);

                $browser->visit('/food/list/cheap')             // but of course, show the items that less than 30€
                    ->assertSeeIn(".list-group", $checkTitle);
            } else  {
                $browser->visit('/food/list/expensive')         // show items over 30€ when clicking the 'expensive' link
                    ->assertSeeIn(".list-group", $checkTitle);

                $browser->visit('/food/list/cheap')             // but of course, don't show items under 30€
                    ->assertDontSeeIn(".list-group", $checkTitle);
            }

        });
    }
}
